<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->truncate();
		// Deefault login user
		App\User::create([
			'name' => 'Devs webserve',
			'email' =>'devs@gmail.com',
			'password' => bcrypt('Abcdef12345-')
		]);

		// Create new 10 users
		for($i = 0; $i < 10; $i ++) {
			App\User::create([
				'name' => 'Devs '. $i,
				'email' =>'devs'.$i.'@gmail.com',
				'password' => bcrypt('Abcdef12345-')
			]);
		}	
	}
}
