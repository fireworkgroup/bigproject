## Sunfolower Project

# Setup poject

#### Laravel 5.0

#### PHP >= 5.4, PHP < 7

#### Xamp 3.2.1

# Step by step

### Xamp config 
	
Change root folder

- Go to: `C:\xampp\apache\conf\httpd.conf`

- Open `httpd.conf`

- Find tag DocumentRoot `C:/xampp/htdocs`

- Edit tag to DocumentRoot `Your project folder`
	- Ex: sunfolower_firework
- Restart Your Apache

### Run project
	
- Clone project from : 
    - https://ythoang@gitlab.com/fireworkgroup/bigproject.git
    
- Composer install: cd `your folder` and run `composer install`

- Open browser and run `localhost` to run your site

# Project's structure

### View

- assets
	- All css contents. Follow [LESS](http://lesscss.org) syntax and split folders
	- run `gulp` to build less into css.
	- run `gulp watch` to rebuild less to css and auto reload.
- auth

- emails

- errors

- `includes` 
	
	- `All public contents. Ex: header, footer..`

- `pages`

	- `All pages of app. Ex: home page, contact page..`

- app.blade.php
	- Default layout of app.