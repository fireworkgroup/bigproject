<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model {

	protected $table='events';

	protected $table=['title','content','date_start','date_end','image','user_id'];

	public $timestamps= true;

	public function user (){

		 return $this->belongTo('App\User');
	}




}
