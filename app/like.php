<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class like extends Model {

	protected $table='likes';

	protected $table=['user_id','event_id','comment_id'];

	public $timestamps= true;
}
