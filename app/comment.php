<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model {

	protected $table='comments';

	protected $table=['content','user_id','event_id'];

	public $timestamps= true;
}
