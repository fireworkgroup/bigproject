<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table='categories';

	protected $fillable=['id','name','description','image','parent_id','user_id'];

	public $timestamps= true;


	public function post()
	{
		return $this->hasMany('App\User');
	}

	
}


