<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Show login's form
Route::get('login','UsersController@loginForm');

// Submit information to login
Route::post('login', 'UsersController@login');

// Logout user 
Route::get('logout', 'UsersController@logout');

Route::get('register', 'UsersController@registerForm');

Route::post('register', 'UsersController@register');

//Add event into database
Route::group(['prefix'=>'admin'], function(){
	Route::group(['prefix'=>'event'], function(){
		Route::get('add', ['as'=>'admin.event.getAdd', 'uses'=>'EventsController@addForm']);
		Route::post('add', ['as'=>'admin.event.postAdd', 'uses'=>'EventsController@view']);
		Route::get('show/{id}', 'EventsController@show');
	});
});
	Route::group(['prefix'=>'categories'], function(){
		Route::get('list','CategoriesController@listCategory');
		Route::get('add', ['as'=>'categories.addCategory', 'uses'=>'CategoriesController@addCategory']);
		Route::post('add', ['as'=>'categories.postCategory', 'uses'=>'CategoriesController@postCategory']);
	});

