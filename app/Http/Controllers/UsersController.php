<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;

class UsersController extends Controller
{
	
	// Render form login
	public function loginForm() {
		if (!Auth::guest()) {
			return redirect()->intended('/');
		} else {
			return view('pages.login');
		}
	}

	// Validation and login with exist user
	public function login(Request $request) {
		$rules = [
			'email' =>'required|email',
			'password' => 'required|min:8'
		];
		$messages = [
			'email.required' => 'Email là trường bắt buộc',
			'email.email' => 'Email không đúng định dạng',
			'password.required' => 'Mật khẩu là trường bắt buộc',
			'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
		];
		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		} else {
			$email = $request->input('email');
			$password = $request->input('password');

			if( Auth::attempt(['email' => $email, 'password' =>$password])) {
				return redirect()->intended('/');
			} else {
				$errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
				return redirect()->intended('/login')->withInput()->withErrors($errors);
			}
		}
	}

	// Logout system 
	public function logout() {
		\Auth::logout();
		return view('pages.welcome');
	}
}