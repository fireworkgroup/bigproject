<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Auth;
class CategoriesController extends Controller {

	public function listCategory()
	{
		$category= Category::all();
	    return view('categories.list')->with('category', $category);
	}

	public function addCategory()
	{
		$category = Category::all();
    	return view('categories.add');
	}
	public function postCategory(Request $request)
	{
		$this->validate($request, array(
			'name'=>'required|max:225',
			'description'=>'required'
			));

		$category = new Category;
		$category->name = $request->name;
		$category->description = $request->description;
		$category->parent_id = 1;
        $category->user_id = Auth::user()->id;

		if($request->hasFile('image')) {
        	$file=$request->file('image');
        	$name= $file->getClientOriginalName();
        	$image=time().'_'.$name;
        	while(file_exists("upload/category/".$image)){
        		$image=time().'_'.$name;
        	}
        	$file->move("upload/category/",$image);
        	$category->image=$image;
        }
        else {
        	$category->image="";
        }
           $category->save();
           
     }
}
	

