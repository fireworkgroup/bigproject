	@extends('app')
	@section('content')
	<div class="container-fluid">
			<div class="row">
			<div class="col-md-8">
				<h1 class="page-header">
					<small>Add Category</small>				
				</h1>	
			</div>

			<div class="col-lg-7" style="padding-bottom:120px">
				<form class="form_add_event" action="{!! url('/categories/add') !!}" method="POST" accept="utf-8" files="true" enctype="multipart/form-data">
					<input type="hidden" value="{{ csrf_token() }}" name="_token">
					<div class="form-group">
						<label">Category Name</label>
						<input class="form-control" name="name">
					</div>	
					<div class="form-group">
						<label">Category Description</label>
						<textarea name="description" rows ="5" cols="100"></textarea>
					</div>
					<div class="form-group">
						<label">Category Image</label>
						<input type="file" class="form-control" name="image">
					</div>				
				<div class="button">
					<button align="center" type="submit" class="btn btn-primary">Add Category</button>
					<button type="reset" class="btn btn-primary">Cancel</button>		
				</div>	
				</form>
				
			</div>
			
		</div>
		
	</div>