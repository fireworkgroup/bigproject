@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">

		<div class="col-md-9">
			<h1 class="page-header">
				<small>List Category</small>				
			</h1>	
		<table class ="table table-striped table-bordered table-hover" id ="table-category">
			<thead>
				<tr>
					<th>Parent ID</th>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Image</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($category as $cate)
				<tr>
				<th>{{ $cate->parent_id }}</th>
				<th>{{ $cate->id }}</th>
				<th>{{ $cate->name }}</th>
				<th>{{ $cate->description }}</th>
				<th><img style="width:200px; height:200px;" src="{{asset('upload/category/'.$cate->image) }}"></th>
				<th class="center"><i class="fa fa-trash-o fa-fw">Delete<a href=""></a></i></th>
				<th class="center"><i class="fa fa-trash-o fa-fw">Edit</i></th>
				</tr>
			@endforeach			 
			</tbody>		
		</div>		
		</table>	

	</div>
</div>


@endsection

