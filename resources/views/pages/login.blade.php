@extends('app')
@section('content')
<div class="container">
	<div class="card card-container">
		<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
		<p id="profile-name" class="profile-name-card"></p>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form class="form-signin" role="form" method="POST" action="{{url('login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<span id="reauth-email" class="reauth-email"></span>
			<input type="email" id="email" class="form-control" placeholder="Email address" required autofocus name="email" value="{{ old('email') }}">
			<input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
			<div id="remember" class="checkbox">
				<label>
					<input type="checkbox" value="remember-me"> Remember me
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login in</button>
		</form>
		<!-- <a href="" class="forgot-password">
			Forgot the password?
		</a> -->
	</div>
</div>
@endsection