@extends('app')

@section('content')
<!-- <div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div>
	</div>
</div> -->
<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3" style="left:-15px;">
@include('includes.menu-left')
</div>

   <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 main">
@yield('homeContent')
         </div>
@endsection
